﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/master/Principal.Master"
    CodeBehind="servicos.aspx.vb" Inherits="CartorioMarcioMilani2010.servicos" Title="Serviços | 25º Tabelião de Notas Millani" %>

<%@ Register Src="controls/menu_servicos.ascx" TagName="menu_servicos" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="O 25º Tabelião de Notas Millani presta serviços como escrituras, procurações, autenticações e reconhecimentos, separação e divórcio, inventário e partilha, atas notariais e testamentos." />
    <meta name="keywords" content="atas notariais,autenticar,autenticacao,autenticação,autenticaçao,escrituras públicas,escrituras publicas,escritura pública,inventário,inventários,partilha,procurações,procucacoes,procuração,procuracao,separação e divórcio,testamento,testamentos," />
    <link rel="canonical" href="http://www.25serviconotarial.com.br/servicos.aspx" />
    <link href="<%=ConfigurationManager.AppSettings("bsHtml") %>css/servicos.css" rel="stylesheet"
        type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:menu_servicos ID="menu_servicos1" runat="server" />
    <h1>
        Nossos Serviços</h1>
    <div class="pg_servicos">
        <a name="escrituras" rel="nofollow"></a>
        <h2>
            Escrituras Públicas</h2>
        <p>
            <strong>O que é?</strong><br />
            A escritura pública é um ato praticado perante o notário, que contém a manifestação
            de vontade das partes em realizar um negócio ou declarar uma situação juridicamente
            relevante.<br />
            <br />
            O notário antes de lavrar uma escritura, ouve qual é a necessidade das partes e
            as aconselha, apresentando a melhor solução jurídica para o que pretendem. Nesse
            processo também verifica a licitude (possibilidade jurídica) do negócio, identifica
            as pessoas que dele participam e avalia suas capacidades civis, analisa os documentos
            exigidos, e por fim, traduz a vontade das partes para a linguagem técnica jurídica
            da escritura pública.<br />
            <br />
            A fé pública do notário confere às escrituras o "status" de provas pré-constituídas,
            e todo seu conteúdo é acatado como verdadeiro, garantindo maior segurança ao negócio
            jurídico e reduzindo a quantidade de disputas judiciais<br />
            <br />
            <strong>Aplicações</strong><br />
            A lei exige o uso da escritura pública nas alienações imobiliárias, que em geral
            resultam em altos valores monetários e em atos que exigem publicidade ou podem afetar
            direitos de terceiros.<br />
            <br />
            Exemplos de atos que <strong>devem</strong> ser feitos por escritura pública:
            <br />
            <br />
            <span style="margin-left: 30px;">&#9679; Transferência de direitos sobre imóveis (compra
                e venda, doação, usufruto, etc.);</span><br />
            <span style="margin-left: 30px;">&#9679; Divisão de área amigável;</span><br />
            <span style="margin-left: 30px;">&#9679; Emancipação de menor;</span><br />
            <span style="margin-left: 30px;">&#9679; Pacto antenupcial para o casamento;</span><br />
            <span style="margin-left: 30px;">&#9679; Declarações públicas;</span><br />
            <br />
            No entanto, nada impede que as partes, querendo garantir maior segurança a um contrato
            de natureza particular, o façam por escritura pública. Com isto, o contrato fica
            arquivado nos livros do tabelionato, e pode ser acessado a qualquer momento por
            meio de uma certidão, evitando "dores de cabeça", tais como o extravio do documento,
            ou uma arguição de falsidade em processo judicial.<br />
            <br />
            <strong>Certidões</strong><br />
            Por terem natureza pública, as escrituras são registradas em livros próprios, e
            permanecem armazenadas eternamente para consultas futuras ou para emissões de certidão
            a qualquer interessado.<br />
            <br />
            Para agilizar o processo de busca e emissão de certidões aos usuários, contamos
            com sistema informatizado, que permite a expedição da certidão em questão de minutos.<br />
            <br />
            <strong>Custo</strong><br />
            Os cartórios obedecem a tabela de custas do Estado de São Paulo.<br />
            <br />
            <strong>Documentos necessários</strong><br />
            Para as escrituras de declaração simples, bastam os documentos pessoais dos declarantes.<br />
            <br />
            <strong><i>Pessoa física:</i></strong><br />
            <span style="margin-left: 30px;">1. RG, Carteira de Identidade Profissional ou Passaporte;</span><br />
            <span style="margin-left: 30px;">2. CPF;</span><br />
            <span style="margin-left: 30px;">3. Certidão de casamento, caso a pessoa já tenha casado,
                mesmo que já esteja separada ou divorciada;</span><br />
            <br />
            <strong><i>Pessoa Jurídica:</i></strong><br />
            <span style="margin-left: 30px;">1. Contrato ou Estatuto Social da empresa com as alterações
                promovidas ou sua consolidação na junta comercial;</span><br />
            <span style="margin-left: 30px;">2. Itens 1 e 2 de pessoa física para os sócios ou representantes
                legais;</span><br />
            <span style="margin-left: 30px;">3. Certidão simplificada da Junta Comercial atualizada
                até 30 dias.</span><br />
            <br />
            Entretanto, cada tipo de escritura tem uma particularidade, e os documentos necessários
            variam conforme o negócio. A compra e venda de imóveis e a transmissão de direitos
            sobre imóveis, por exemplo, exige uma série de documentos, como matrícula atualizada
            do imóvel, certidões de processos contra o imóvel e contra os vendedores, negativas
            de débitos tributários, etc.<br />
            <br />
            <strong>Separações, Divórcios, Inventários e Partilhas</strong><br />
            Com a nova atribuição estabelecida aos notários pela Lei Federal nº 11.441/2007,
            as pessoas poderão realizar inventários e partilhas, bem como separações e divórcios
            por escritura pública nos Tabelionatos, sem a obrigatoriedade de passar pelo Poder
            Judiciário.<br />
            <br />
            <asp:HyperLink ID="HyperLink17" runat="server" NavigateUrl="#topo" rel="nofollow"
                Style="float: right;"><strong>voltar ao topo &raquo;</strong></asp:HyperLink>
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/contato.aspx" rel="nofollow"><strong>&laquo; Solicite um contato</strong></asp:HyperLink>
            <br />
        </p>
    </div>
    <div class="pg_servicos">
        <a name="procuracoes"></a>
        <h2>
            Procurações</h2>
        <p>
            Leciona Oldemir Bilhalva Teixeira,em seu livro Princípios E Procedimentos Notarias
            que a procuração pública feita por notário é uma espécie de escritura pública, à
            qual se aplicam, no que couberem, os requisitos gerais das escrituras públicas.
            A procuração pública é um negócio jurídico unilateral que se instrumentaliza por
            meio de escritura pública, com características especiais que justificam o seu estudo
            em separado. Em sendo ato unilateral, somente aquele confere os poderes de representação,
            chamado de "outorgante" assina a procuração. A aquele a quem são confiados os poderes
            de representação, denominado "procurador" não precisa comparecer nem assinar o ato
            notarial, citando Brandelli.
            <br />
            <br />
            <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="#topo" rel="nofollow"
                Style="float: right;"><strong>voltar ao topo &raquo;</strong></asp:HyperLink>
            <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/contato.aspx" rel="nofollow"><strong>&laquo; Solicite um contato</strong></asp:HyperLink>
            <br />
        </p>
    </div>
    <div class="pg_servicos">
        <a name="reconhecimentos" rel="nofollow"></a>
        <h2>
            Autenticações e Reconhecimentos de Firma</h2>
        <p>
            <strong>O que é?</strong><br />
            O reconhecimento de firmas é uma declaração pela qual o notário, ou um de seus substitutos
            e escreventes, confirma a autenticidade ou semelhança da assinatura de determinada
            pessoa em um documento.<br />
            Existem duas formas de se reconhecer uma assinatura: por semelhança e por autenticidade
            (popularmente "por verdadeiro").<br />
            <br />
            <strong>Reconhecimento por semelhança</strong><br />
            A assinatura é comparada com uma ficha de assinatura que a pessoa tem arquivada
            no tabelionato, e se o notário ou seu escrevente as considerarem semelhantes, a
            firma é reconhecida por semelhança. Para executar esta atividade com confiabilidade,
            o Tabelionato Milani mantém sua equipe constantemente atualizada em grafotécnica
            (estudo técnico da grafia das assinaturas) e na detecção de falsificações de documentos.<br />
            <br />
            O reconhecimento por semelhança é bastante prático para pessoas que estão sempre
            em transito ou quando não têm a disponibilidade de tempo para ir até um tabelionato.
            Mas nem sempre é possível utilizar esta forma, pois existem restrições de aceitação.
            Nos títulos cambiários (notas promissórias, etc.) e nos documentos de transferência
            de veículo automotor (determinação do Contran), por exemplo, é necessário o comparecimento
            pessoal do signatário.<br />
            <br />
            Antes de reconhecer uma assinatura, verifique junto a quem a exigiu se aceita o
            reconhecimento por semelhança.<br />
            <br />
            <strong>Reconhecimento por autenticidade (por verdadeiro)</strong><br />
            Para o reconhecimento por autenticidade, o signatário do documento deverá comparecer
            obrigatoriamente perante o notário que certificará, mediante apresentação de identificação
            com foto, que a assinatura aposta no documento é autêntica, ou seja, certificará
            que o próprio signatário assinou o documento.<br />
            <br />
            O reconhecimento autêntico é muito mais seguro para o destinatário do documento
            que o método por semelhança, porque pressupõe uma responsabilidade maior do notário,
            que confirmou pessoalmente a identidade do signatário.<br />
            <br />
            O Tabelionato Milani para garantir ainda maior segurança no reconhecimento por autenticidade,
            utiliza um sistema informatizado de verificação de impressões digitais. Está cientificamente
            comprovado que não existem duas impressões digitais identicas para pessoas diferentes,
            o que reduz a possibilidade de fraude no reconhecimento de assinatura na forma autêntica.<br />
            <br />
            <strong>Para que serve reconhecimento de firma em cartório?</strong><br />
            <br />
            <strong>Para comodidade da população.</strong><br />
            Já imaginou ter que ir pessoalmente até o Departamento de Transito para registrar
            a transferência de titularidade de um veículo? Isto ocorreria se não existisse o
            reconhecimento de firmas em cartório, pois o funcionário público exigiria que o
            portador do documento assinasse em sua presença, comprovando a identidade de quem
            assinou.<br />
            <br />
            <strong>Para segurança dos negócios.</strong><br />
            Imagine agora a assinatura não reconhecida em um contrato particular. Que garantia
            se teria em relação à aquela assinatura? Sua veracidade poderia se discutida judicialmente,
            e seria necessário uma perícia grafotécnica (100 vezes mais custosa que um reconhecimento
            de firma) para confirmar a assinatura.<br />
            <br />
            <strong>Procedimento para cadastrar uma ficha de assinatura</strong><br />
            Cadastrar uma ficha de assinatura ou atualizá-la não tem custo nenhum, mas é necessário
            que o interessado venha ao tabelionato acompanhado da seguinte documentação:<br />
            <br />
            <span style="margin-left: 30px;">1. RG, Carteira de Identidade Profissional ou Carteira
                Nacional de Habilitação (com foto);</span><br />
            <span style="margin-left: 30px;">2. CPF;</span><br />
            <span style="margin-left: 30px;">3. Certidão de casamento, caso a pessoa já tenha casado,
                mesmo que já esteja separada ou divorciada</span>
            <br />
            <br />
            <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="#topo" rel="nofollow"
                Style="float: right;"><strong>voltar ao topo &raquo;</strong></asp:HyperLink>
            <asp:HyperLink ID="HyperLink7" runat="server" NavigateUrl="~/contato.aspx" rel="nofollow"><strong>&laquo; Solicite um contato</strong></asp:HyperLink>
            <br />
        </p>
    </div>
    <div class="pg_servicos">
        <a name="separacao" rel="nofollow"></a>
        <h2>
            Divórcio Extrajudicial / Inventario e Partilha</h2>
        <p>
            <span style="color: #F00;"><em>Por: Douglas de Campos Gavazzi</em></span><br />
            <strong>A desjudicialização do judiciário</strong><br />
            <br />
            A reforma do judiciário, tão esperada, foi uma resposta e uma constatação de soluções
            para a morosidade da Justiça. O direito nasceu para trazer a paz social, mas durante
            toda sua formação, (o que se demonstra todos os dias com o processo legislativo
            e a representatividade em uma Estado Democrático de Direito) tornou-se um instrumento
            tão complexo, que o seu destinatário final, não mais o reconhece e às vezes se abstém
            de exercê-lo pela morosidade que o judiciário o proporciona. A Morosidade de justiça,
            acaba por trazer à seus destinatários, verdadeira injustiça.<br />
            <br />
            O acesso à justiça, constitucionalmente previsto no texto da Carta Magna em seu
            contemplado artigo 5º, especificamente no inciso XXXV, é moroso e dentre diversos
            motivos proporcionados por formalidades precípuas, também é decorrente do excesso
            de processos empilhados às estantes dos cartórios judiciais ao relento da poeira
            e de alguns de seus servidores desqualificados e acomodados à máquina judicial.<br />
            <br />
            Criou-se, portanto um grande círculo vicioso. As obrigações pessoais e contratadas
            não são cumpridas espontaneamente porque a prestação jurisdicional tarda, pois o
            volume de processos é desproporcional à capacidade de julgar, e para se quebrar
            esse círculo é preciso facilitar o acesso à Justiça. A reforma no Poder Judiciário,
            não é uma, mas trata-se de várias reformas.<br />
            <br />
            A reforma do Judiciário, ocorreu com a Emenda Constitucional nº 45. No entanto,
            entendo que ali apenas iniciou-se uma grande trabalho de reformas processuais que
            certamente amenizarão o tal vício que consubstancia a morosidade do judiciário.
            Podemos identificar tais reformas com as modificações introduzidas no Código de
            Processo Civil Brasileiro, que por si só é um grande instrumento basilar e análogo
            e subsidiário à outras justiças, tais como a do Trabalho.<br />
            <br />
            A Lei nº. 11.441/07 foi uma reforma processual. Com ela, se alterou dispositivos
            do Código Processual Brasileiro e deu margem aos seus destinatários à livre escolha
            a busca da solução de seus devaneios jurídicos. À essa legislação federal atribuo
            importância correlativa à ocasião em que foi promulgada a lei 6.515/77 que naquela
            ocasião conferiu aos "descasados" a oportunidade de regularizar suas situações conjugais
            com base moral e legal na lei.<br />
            <br />
            A lei objeto deste estudo é de relevante importância à sociedade, pois mais uma
            vez, seus destinatários poderão satisfazer seus desejos regulares através de procedimentos
            mais simplificados e com a mesma segurança conduzida pelo judiciário.<br />
            <br />
            O judiciário vem se aprimorando, e com ele vem a desjudicialização. As questões
            incontroversas estão sendo "expurgadas" da apreciação judicial por motivos óbvios.
            Se não há litígio, não há lide, portanto, se não há lide, motivos não justificam
            a provocação do judiciário para resolução de questões de cunho simplesmente homologatórias.
            Que liberdade consensual é essa, cuja vontade deve estar fomentada por uma decisão
            judicial declaratória?<br />
            <br />
            A lei veio a auxiliar o "desafogamento" do judiciário e dessa vontade compartilho
            e fundamento este estudo. A mudança tem causado resistência por parte de alguns
            advogados, bem como de algumas instituições que até mesmo recusam a orientar seus
            clientes a optarem pela via extrajudicial para a solução de seus interesses comuns.
            Toda mudança causa certa resistência. O inteligível é persistir com a burocratização
            judicializada.<br />
            <br />
            Houve quem escrevesse que a desjudicialização das separações e divórcios é estritamente
            prejudicial à sociedade, pois estimula a dissolução familiar em razão da facilidade
            dos procedimentos e ainda, mostra-se como uma “porta aberta” à realização de fraudes,
            como por exemplo, a credores não habilitados. A própria OAB, que deveria ser defensora
            do aprimoramento judicial, no início da lei à ela se demonstrou contrária, como
            salienta a então vice-presidente da entidade, Márcia Regina Machado Melaré.<br />
            <br />
            <span style="margin-left: 30px;">"O 'desafogamento' do Judiciário deveria se dar por
                outras formas". "Além disso, com a ausência de um juiz, a responsabilidade do advogado
                será dobrada, já que as partes não terão a segurança que a justiça proporciona".</span><br />
            <br />
            A sociedade roga pela continuidade da desjudicialização de determinados processos,
            para a contribuição à reforma do Poder Judiciário, ressocializando os anseios sócio-econômicos,
            promovendo novas condições infra-burocráticas e cada vez mais favoráveis aos destinatários
            da justiça, pois a desjudicialização não remove o sentimento de justiça, mas traz
            de volta aos seus beneficiários o sentimento de verdadeira satisfação e do justo.
            Justiça sim. Morosidade e burocratização homologatória não.<br />
            <br />
            <strong>A facultatividade da aplicação da lei 11.441/07</strong><br />
            <br />
            A lei federal 11.441/07 foi promulgada em 04 de janeiro de 2007, e com ela nasceu
            uma nova possibilidade para a realização de inventários, separações e divórcios,
            desde que as partes sejam maiores, capazes e concordes.<br />
            <br />
            Referida lei, alterou e acrescentou dispositivos ao CPC, criando por tanto, uma
            faculdade, ou seja, uma opção às partes em propor ação judicial ou dar início à
            um procedimento extrajudicial. Não há, portanto, obrigatoriedade de exaurimento
            das possibilidades administrativas para então perquerir o processo judicial. É livre
            a escolha da parte.<br />
            <br />
            Primordial, aqui, é a análise do caso, feita pelo assistente jurídico. Certamente
            será o advogado, então, que indicará à seu cliente, a melhor via a ser seguida mediante
            a situação lhe apresentada.<br />
            <br />
            Qualquer obrigatoriedade no sentido de forçar objetivamente a opção pela via extrajudicial,
            é absolutamente inconstitucional, com apreço no princípio da inafastabilidade do
            Poder Judiciário.<br />
            <br />
            A opção criada pela nova lei, é fruto da conveniência objetivada na reforma do judiciário,
            ou seja, promover o desafogamento dos tribunais de processos consensuais e simplesmente
            homologatórios. A criação de leis que disponibilizam a opção pela via administrativa
            não é privilégio dos anseios da emenda constitucional 45, mas sim, alvo de uma massa
            global e mundial que vem observando uma preferência pelo "mundo extrajudicial",
            como se pode exemplificar, a Lei de Arbitragem, que muito embora no Brasil, apesar
            dos seus anos de vigência, não tem grande expectro de aplicação, em outros países
            é amplamente aplicada, inclusive com repercussão internacional e na aplicação de
            impasses diplomáticos.<br />
            <br />
            Na forma então analisada, versa o professor Ezequiel Morais sobre essa faculdade:<br />
            <br />
            <span style="margin-left: 30px;">"Entretanto, inexiste óbice, oportuno frisar, para
                o surgimento e aperfeiçoamento de mecanismos extrajudiciais que possam versar sobre
                a administração pública de direitos de ordem privada, desde que não seja excluída
                a opção pelo Poder Judiciário. Tanto é verdade que a Lei 9.307/96 (arbitragem) mostra-se
                como grande exemplo: não foi declarada inconstitucional, não há necessidade de homologação
                judicial e nem afastou o direito do indivíduo de acessar o Judiciário"</span><br />
            <br />
            O próprio texto da lei, é claro em prescrever que poderão ser os atos praticados
            por escritura pública, e não determinam via necessária. Excessão é quando as partes
            não são concordes, ou quando os herdeiros do falecido são menores ou incapazes.<br />
            <br />
            Também, no mesmo plano, quando na separação ou divórcio, há interesse de filhos
            menores. Nesses casos apresentados, obrigartoriamente há necessidade de ação judicial.
            Ao juiz caberá solucionar a lide dos cônjuges ou dos herdeiros e ainda fixar a guarda
            dos filhos menores. Ao Ministério Público Estadual, caberá a verificação dos interesses
            dos menores envolvidos.<br />
            <br />
            Também há obrigatoriedade de propositura de ação judicial, ocasiões em que o de
            cujus deixou testamento. Entendimento diverso, demonstra a assessora jurídica do
            Colégio Notarial do Brasil – Conselho Federal, Karin Regina Rick Rosa.<br />
            <br />
            <span style="margin-left: 30px;">"(...) Essa Aliás, era a realidade anterior à vigência
                da lei, quando o notário já lavrava escrituras públicas de partilha, independentemente
                de existir ou não testamento, devendo os interessados submetê-las à homologação
                pelo juiz. De modo que, se antes da vigência da lei era possível a prática do ato
                notarial, não há razão para sustentar que desde o dia 5 de janeiro de 2007 não é
                mais possível a realização de inventário e partilha por escritura pública quando
                houver testamento, devendo ficar a ressalva de que, nesses casos, a escritura precisa
                ser homologada judicialmente."</span><br />
            <br />
            Ainda que o notário seja um profissional do Direito, e à ele tenha sido incumbida
            pela Lei Federal 8.935/94 a competência para a realização e orientação na lavratura
            de testamentos públicos, aqui também se demonstra uma exceção, de forma que em havendo
            testamento, os inventários obrigatoriamente deverão ser submetidos ao fólio judicial.<br />
            <br />
            É pacífico, portanto, que a aplicação da legislação federal 11.441/07 é facultativa.
            Casos haverão em que as partes optarão em propor ação judicial ainda que preencham
            todos os requisitos desta lei.<br />
            <br />
            Como já dito, o advogado tem fundamental importância na instrução das partes a optar
            pelos caminhos oferecidos pela justiça, seja judicial ou extrajudicial, uma vez
            que os interessados, geralmente leigos, não tem o conhecimento jurídico técnico
            necessário para optar por suas próprias razões. O advogado deve instruí-los.<br />
            <br />
            Para demonstrar eficazmente a aplicabilidade do ordenamento objeto deste trabalho
            jurídico, dicorrerei a seguir sobre cada uma das possibilidades de sua efetiva aplicação
            legal.<br />
            <br />
            ----<br />
            <br />
            <span style="color: #F00;"><em>Por: Tatiani Calderaro Dalcin. Tabeliã e Registradora
                Substituta da Serventia Notarial e Registral de Progresso/RS///Formanda em Direito
                pela UNIVATES, Lajeado/R</em></span><br />
            fonte:
            <asp:HyperLink ID="HyperLink12" runat="server" NavigateUrl="http://www.webartigos.com/articles/30049/1/As-alteracoes-trazidas-pela-Lei-114412007-como-forma-alternativa-de-acesso-a-justica/pagina1.html"
                Target="_blank">http://www.webartigos.com/articles/30049/1/As-alteracoes-trazidas-pela-Lei-114412007-como-forma-alternativa-de-acesso-a-justica/pagina1.html</asp:HyperLink><br />
            <br />
            <strong>As alterações trazidas pela Lei 11.441/2007 como forma alternativa de acesso
                à justiça</strong><br />
            <br />
            Resumo:<br />
            O ordenamento jurídico há muito reclama por procedimentos que sejam céleres e contribuam
            para desafogar o judiciário. Este trabalho tem como escopo mostrar as alterações
            que a Lei 11.441 de 04 de Janeiro de 2007, introduziu no ordenamento jurídico, a
            qual é de grande relevância social, pois altera dispositivos do Código de Processo
            Civil, possibilitando a realização de inventário, partilha, separação consensual
            e divórcio consensual por via administrativa - desde que observadas as condições
            expressas na nova lei - procedimentos anteriormente determinados somente pelas vias
            judiciais. Nesse sentido, fez-se uma análise sobre a exigência desse tipo de legislação,
            que faculta a utilização da via extrajudicial - no caso em tela, especificamente
            no que tange a realização dos referidos atos junto ao tabelionato de notas, na realização
            de escrituras públicas - além de reavaliar a competência-chave do Judiciário. Procurou-se
            analisar se a medida atingiu os objetivos a que se propôs: agilizar, simplificar
            e reduzir custos no atendimento à população, além do descongestionar o Judiciário.
            Este humilde trabalho não tem a pretensão de esgotar o tema, mas busca expor um
            novo olhar sobre o assunto, esperando poder ajudar, de alguma forma, nesse perene
            mister.<br />
            <br />
            Com frequência, têm sido realizadas modificações na legislação processual, com o
            intuito de simplificar os trâmites processuais e baratear os custos de certos atos
            jurídicos. De fato, somente nos últimos três anos foram editadas quase dez leis
            para modificar o Código de Processo Civil, sendo que uma das últimas alterações
            foi realizada por meio da Lei n. 11.441, publicada em 05 de janeiro de 2007. O citado
            diploma normativo, que nem período de vacatio legis teve, modificou o Código Civil
            para possibilitar a realização do inventário, da separação e do divórcio consensuais
            pela via administrativa.<br />
            <br />
            Por outras palavras: o inventário, a separação e o divórcio, que durante tantos
            anos demandaram para sua realização a manifestação de um magistrado, podem, agora,
            nos casos previstos em Lei, ser realizados por meio de escritura pública, a ser
            lavrada pelo tabelião. Afastou-se, portanto, a necessidade de - em tais casos -
            haver pronunciamento jurisdicional para que seja realizado o inventário ou mesmo
            a dissolução da sociedade conjugal ou do vínculo matrimonial.<br />
            <br />
            O fundamento norteador da elaboração da Lei 11.441/07 - que já vem sendo chamada
            de "Nova Lei de Divórcio" apesar de abranger também o inventário por sucessão e
            a partilha de bens - está consignado no parágrafo inicial do “Pacto de Estado em
            Favor de um Judiciário mais Rápido e Republicano”, publicado no DOU em dezembro
            de 2004 elaborado em conjunto e assinado pelos Presidentes dos três Poderes da República.
            Tal Pacto, em seu parágrafo inicial, traz à tona que poucos problemas nacionais
            possuem tanto consenso no tocante aos diagnósticos quanto à questão judiciária.
            A morosidade dos processos judiciais e a baixa eficácia de suas decisões retardam
            o desenvolvimento nacional, desestimulam investimentos, propiciam a inadimplência,
            geram impunidade e solapam a crença dos cidadãos no regime democrático.<br />
            <br />
            Neste contexto, evidenciam-se as soluções alternativas para a solução dos conflitos
            - principalmente na seara da Mediação, Arbitragem, Conciliação e Procedimentos Administrativos
            ou Extrajudiciais - desmistificando-se a idéia do "acesso à justiça" apenas como
            "acesso ao Poder Judiciário". Existe justiça fora dos tribunais. Ademais, fica claro
            que existe sim, a busca pelo aumento da qualidade dos serviços prestados aos usuários
            do sistema jurídico, bem como o equilíbrio entre tempo e o alcance do direito buscado.
            É imprescindível se alcançar uma atuação mais pronta e eficaz da justiça, eliminando
            certas mazelas características do Judiciário brasileiro, além de coibir a árdua
            demora na entrega da prestação jurisdicional que, juntamente com a injuriosa burocracia,
            são elementos perniciosos à pacificação social e ferem de forma fulminante o direito
            ao acesso à justiça.<br />
            <br />
            Com efeito, há a necessidade de que a prestação da tutela jurisdicional atenda à
            realidade sócio-jurídica a que se destina, atuando como instrumento à efetiva realização
            de direitos, uma vez que não basta facilitar o ingresso à justiça a todos que acreditam
            ser detentores do direito material. Deve-se, acima de tudo, buscar, de forma efetiva
            e tempestiva, produzir soluções satisfatórias para os que dela necessitem. É imprescindível
            atender a todos e solucionar os conflitos de todos com qualidade, pacificando com
            agilidade.<br />
            <br />
            As inovações no âmbito social ocasionadas pela Lei 11.441/2007 - principalmente
            nos casos de separação e divórcio sem a intervenção do Estado-Juiz na vida dos cônjuges,
            quando presentes os requisitos de validade - de fato vieram a facilitar tal ato
            consensual, poupando as partes de maiores constrangimentos nessa decisão não litigiosa.<br />
            <br />
            Utilizar-se a via administrativa ou extrajudicial, nos casos permitidos em lei,
            é imperiosa para se evitar os transtornos de espera de uma ação judicial, que sempre
            gera aos jurisdicionados uma demora, uma incerteza, acarretando prejuízos de difícil
            reparação quando se necessita desta prestação jurisdicional urgente. Optar-se pela
            solução extrajudicial viabiliza ao Poder Judiciário ganho de tempo para se dedicar
            às decisões de questões mais complexas.<br />
            <br />
            Assim, a nova lei, apesar de algumas dificuldades que ainda a acompanham quando
            da sua aplicação, deu um grande avanço à negociação e à mediação familiar, permitindo
            ao casal já maduro na sua decisão de ruptura matrimonial, efetivar seu procedimento
            de separação e divórcio, com assistência do seu advogado, sem precisar se submeter
            a delongas do Judiciário Estadual, em questões menos complexas.<br />
            <br />
            Ainda, importante considerar a facilidade imposta pela lei que, ao eliminar a audiência
            de conciliação obrigatória nos processos judiciais de separação e divórcio, não
            possuiu o condão de vulgarizar o casamento, mas sim, tão somente desburocratizar
            a separação e o divórcio, o que sem dúvidas conseguiu, mantendo-se o acesso à justiça
            e a segurança jurídica.<br />
            <br />
            Sem nenhuma sombra de dúvidas a nova Lei trouxe um grande avanço para o Poder Judiciário,
            que poderá solucionar com maior rapidez processos que realmente envolvam conflitos,
            salientando que quando o fim do casamento ocorre de forma consensual, é totalmente
            dispensável que sua dissolução dependa da intervenção do Juiz.<br />
            <br />
            Quanto à análise dos princípios constitucionais que norteiam o Direito Notarial,
            verifica-se sua eficácia imediata como garantidores da manutenção da segurança jurídica,
            principalmente em face da fé pública do tabelião e da presença indispensável do
            advogado aos atos realizados extrajudicialmente. Nesse sentido, evidente a importância
            de ter o procedimento notarial regras próprias e obedecer aos princípios e à operação
            específica da atividade notarial. Além da função de formalizar a vontade, os tabeliães
            têm a mesma atribuição que teria um juiz em um procedimento judicial desta natureza,
            uma vez que é um profissional do direito, capacitado a formalizar os atos consensuais
            das partes maiores e capazes, e idôneos para facilitar enormemente a vida privada.
            Assim, o valor do documento notarial repousa em sua executividade e eficácia preventiva,
            possibilitando a segurança jurídica.<br />
            <br />
            De tudo isto, tem-se que a sociedade não quer apenas a desburocratização, mas exige
            também segurança em relação aos atos, razão por que é fundamental que se encontre
            o ponto de equilíbrio entre os dois pesos dessa balança. Este é o desafio que os
            cartórios aceitaram para enfrentar a burocracia estatal e oferecer mais celeridade,
            sem pôr em risco a segurança jurídica que os procedimentos buscam garantir.<br />
            <br />
            Portanto, a nova lei não reduz a segurança jurídica dos atos por ela permitidos
            via escritura pública, ampliando o acesso à justiça. O motivo é simples: além de
            todos os princípios norteadores do Direito Notarial, não se excluíram - e nem poderia
            - da apreciação do judiciário os atos dos tabeliães. A separação, divórcio ou inventário
            por escritura pública estão sujeitos ao controle do Poder Judiciário, inclusive
            porque as respectivas serventias são fiscalizadas pela Corregedoria Geral de Justiça.<br />
            <br />
            É, pois, possível concluir que a Lei 11.441/2007 tem sido aprovada e reconhecida
            como um meio efetivo para acessar a justiça, alcançando seu objetivo de simplificação
            de procedimentos, racionalidade e celeridade para as ações, facilitar a vida do
            cidadão e desonerá-lo, desafogar o Poder Judiciário e concentrá-lo na jurisdição
            litigiosa. Outrossim, a inovação disponibiliza aos cidadãos um mecanismo extrajudicial
            rápido, seguro e eficiente para a regularização de situações em que não existe conflito
            entre as partes, preservando-se a segurança jurídica da relação estabelecida.<br />
            <br />
            ----<br />
            <br />
            <span style="color: #F00;"><em>Por: Bela Rita de Cássia Mello Coelho. Tabeliã Substituta
                1º ofício de Boa Vista (RR)</em></span><br />
            fonte:
            <asp:HyperLink ID="HyperLink13" runat="server" NavigateUrl="http://www.ibdfam.org.br/?artigos&artigo=557"
                Target="_blank">http://www.ibdfam.org.br/?artigos&artigo=557</asp:HyperLink><br />
            <br />
            <strong>Reflexões acerca da Lei 11.441/2007</strong>
            <br />
            <br />
            Em 04 de janeiro do ano de 2007 foi promulgada a Lei 11.441/07, que entrou em vigor
            já no dia seguinte, e em três artigos possibilita ao Notário lavrar escrituras públicas
            de separação e de divórcio consensuais, e inventários de forma administrativa, isto
            é, extrajudicialmente em tabelionato de notas.<br />
            <br />
            Para tornar possível esta abordagem buscamos alicerces em nossa Constituição Federal
            vigente, que em seu art. 1°, III, nos brinda com um princípio salutar que é "a dignidade
            da pessoa humana". Este princípio, tido com toda certeza como um princípio fundamental
            e indispensável, lança luzes sobre o tema específico, ora disciplinado pela Lei
            nº 11.441/07.<br />
            <br />
            Identificamos na família o núcleo da tutela da dignidade da pessoa humana, possibilitando
            através dela vislumbrar todo o alcance desta expressão.<br />
            <br />
            Segundo Ricardo Castilho:<br />
            <br />
            <span style="margin-left: 30px;">... a concepção mais correta é a de que na família,
                a tutela da dignidade da pessoa humana, em todo alcance desta expressão, deve ser
                assegurada tanto no curso das relações familiares como diante de seu rompimento,
                cabendo ao direito oferecer instrumentos para impedir a violação a este valor maior.</span><br />
            <br />
            Destarte a nova Lei implica desonerar o Poder Judiciário de processos que não consubstanciem
            verdadeiros litígios, permitindo uma concentração de esforços àqueles casos que
            efetivamente demandem a intervenção judicial.<br />
            <br />
            O ouvir e interpretar a vontade das partes à luz da moral, da justiça e da Lei não
            pode ser entendido como uma mera função material. O grande mestre Carnelutti em
            uma conferência realizada em Madri no ano de 1949 disse: "fosse o Notário um mero
            documentador, estaria fadado a desaparecer, eis que a função de plasmar uma declaração
            de vontade num documento, de modo a que não haja dúvidas sobre a sua autenticidade,
            será realizada por meios mecânicos mais do que suficientes aos fins pretendidos.
            Entretanto, o Notário realiza algo mais, e isto leva à conclusão de que a função
            documentadora constitui o acessório do Notário. A essência da função notarial deve
            ser encontrada em outra forma que forneça, de modo definitivo, sua base, seu conteúdo
            e seu significado. Esta essência está em vias de encontrar-se quando se relaciona
            à idéia de sua missão com a mediação. Vale dizer, quando se parte do estudo da figura
            jurídica do intérprete. Não do intérprete material, e aqui está o mais importante,
            mas do intérprete jurídico. Realmente, o que o Notário faz é interpretar, traduzir
            a realidade social ao campo do Direito, trasladar o fato ao Direito, ligar a Lei
            ao fato".<br />
            <br />
            Visando disciplinar a aplicação da Lei 11.441/07 pelos serviços notariais e de registro
            o Conselho Nacional de Justiça publicou a resolução nº. 35 em 24 de abril de 2007
            assinada pela Ministra Helen Grace que conta com 54 artigos, dirimindo desta forma
            dúvidas e divergências acerca da nova lei.<br />
            <br />
            Desta forma temos uniforme disciplina da nova lei possibilitando a todos formas
            isonômicas a seguir.<br />
            <br />
            <strong>O Art. 982.</strong> do CPC diz de forma explícita "[...] se todos forem
            capazes e concordes, poderá fazer-se o inventário e a partilha por escritura pública,
            a qual constituirá título hábil para o registro imobiliário".<br />
            <br />
            De forma a ser imperativo a vontade das partes sem vícios de consentimento ou maculada
            a vontade dos interessados o que será atestado pelo Notário através da fé-pública
            que lhe é inerente e capacidade civil plena, isto é, possuindo dezoito anos completos
            ou ainda se maiores de dezesseis e menores de dezoito anos mais devidamente emancipados
            pelos pais por escritura pública.<br />
            <br />
            Da mesma forma o Parágrafo único do <strong>Art. 982</strong> do CPC leciona. "O
            Tabelião somente lavrará a escritura pública se todas as partes interessadas estiverem
            assistidas por advogado comum ou advogados de cada uma delas, cuja qualificação
            e assinatura constarão do ato notarial." A figura do advogado é essencial para que
            a escritura atenda os ditames da Lei, o mesmo assinará o ato como assistente, sendo
            parte indispensável.<br />
            <br />
            <strong>Art. 1.124-A.</strong> do CPC "A separação consensual e o divórcio consensual,
            não havendo filhos menores ou incapazes do casal e observados os requisitos legais
            quanto aos prazos" [...], artigo que obriga o Notário a observar requisitos legais
            quanto aos prazos que são o mínimo de um ano de casamento para o caso de separação
            consensual e no que se refere ao divórcio, existe a necessidade de se provar a separação
            há mais de um ano através de sentença judicial transitada em julgada já devidamente
            averbada no Registro Civil do local em que contraíram núpcias ou a Escritura Pública
            de Separação, esta também devidamente averbada, e se separados de fato há mais de
            dois anos, a prova será feita através de documentos que comprovem o fato ou de duas
            testemunhas que participarão e assinarão o ato.<br />
            <br />
            Temos então uma Lei que busca simplificar os procedimentos, ou seja, a Lei é procedimental,
            não altera o direito material.<br />
            <br />
            A Lei possibilitará uma racionalidade e celeridade muito maior o que decorre do
            procedimento Notarial, que por certo será de grande valia äs partes que se encontram
            em consenso, resguardando assim o judiciário para as causas que efetivamente envolvam
            litígio. Desta forma obtém-se celeridade por duas vias: O procedimento Notarial
            é mais rápido e o Judicial também será desonerado de tratar de causas consensuais
            restará mais tempo para tratar de casos litigiosos o que certamente necessita da
            apreciação de quem tem competência para julgar.<br />
            <br />
            <strong>Documentos exigidos para a realização da Escritura de Inventário.</strong><br />
            <br />
            1) Certidão de óbito do autor da herança;<br />
            2) Documento de identidade oficial e CPF das partes e do autor da herança;<br />
            3) Certidão de casamento do cônjuge sobrevivente e dos herdeiros casados (todas
            atualizadas - prazo de 90 dias) e pacto antenupcial, se houver;<br />
            4) certidão de propriedade de bens imóveis e direitos a eles relativos atualizada
            (30 dias) e não anterior a data do óbito;<br />
            5) Documentos necessários á comprovação da titularidade dos bens móveis e direitos,
            se houver;<br />
            6) Certificado de cadastro de imóvel Rural (CCIR), se houver imóvel rural a ser
            partilhado, com a certidão de quitação do imposto territorial rural;<br />
            7) Certidão negativa de tributos municipais que incidam sobre os bens imóveis do
            espólio;<br />
            8) Certidão negativa conjunta da receita Federal e PGFN;<br />
            9) Documentos comprobatórios do domínio e valor dos bens móveis, se houver;<br />
            10) Certidões negativas de ônus reais dos bens do acervo a ser partilhado;<br />
            11) Documentos comprobatório de titularidade dos ativos representados por depósitos
            em contas-correntes, caderneta de poupança, títulos, valores mobiliários, aplicações,
            etc...;<br />
            12) Escritura Pública, na forma exigida na Lei, se houver outorga de poderes para
            ceder e renunciar direitos, apontando o nome do favorecido;<br />
            13) A guia do recolhimento do imposto de transmissão mortis causa ou inter vivos
            (dependendo do caso);<br />
            <br />
            <strong>Documentos necessários para a realização da Escritura de Separação e Divórcio</strong><br />
            <br />
            1) Certidão de Casamento atualizada dos cônjuges (90 dias);<br />
            2) Documento de identidade oficial e CPF/MF de ambos os cônjuges;<br />
            3) Pacto antenupcial, se houver;<br />
            4) Certidão de nascimento ou outro documento de identidade oficial dos filhos absolutamente
            capazes, se houver;<br />
            5) Certidão de propriedade de bens imóveis e direitos a eles relativos se for feita
            a partilha, ou declaração de existência de bens a serem partilhados;<br />
            6) Documentos necessários à comprovação da titularidade dos bens móveis e direitos
            se forem feitas a partilha ou a declaração de inexistência de bens a serem partilhados;<br />
            7) Declaração de que os bens serão partilhados posteriormente, se for o caso;<br />
            8) Comprovante do pagamento do imposto de transmissão inter vivos (se for o caso);<br />
            9) No restabelecimento da sociedade conjugal, certidão de casamento com averbação
            da separação feita no Registro Civil;<br />
            10) Identificação do(s) advogado(s) assistente(s) por meio de carteira da OAB;<br />
            11) Na conversão da Separação em Divórcio, deve ser apresentada, também, certidão
            da sentença de separação Judicial, ou da liminar em separação de corpos ou da escritura
            de separação extrajudicial, para comprovação do lapso temporal;<br />
            12) No divórcio por conversão, deve ser apresentada, também, a averbação da separação
            no respectivo assento do casamento;<br />
            13) Prova documental, se houver, da separação de fato há mais de dois anos, ou algum
            terceiro interveniente que possa comprovar tal situação;<br />
            14) Valor da pensão alimentícia, ou a dispensa dos cônjuges, ou, ainda, a declaração
            de que isto será discutido posteriormente;<br />
            15) Declaração do cônjuge se retornará, ou não, o nome de solteiro (para quem adotou
            o patronímico do outro quando do casamento).
            <br />
            <br />
            <asp:HyperLink ID="HyperLink10" runat="server" NavigateUrl="#topo" rel="nofollow"
                Style="float: right;"><strong>voltar ao topo &raquo;</strong></asp:HyperLink>
            <asp:HyperLink ID="HyperLink11" runat="server" NavigateUrl="~/contato.aspx" rel="nofollow"><strong>&laquo; Solicite um contato</strong></asp:HyperLink>
            <br />
        </p>
    </div>
    <div class="pg_servicos">
        <a name="atas-notariais"></a>
        <h2>
            Atas Notariais</h2>
        <p>
            <strong>O que é?</strong><br />
            Quando pensamos em ata, a primeira idéia que vem à mente é um documento que relata
            sob a forma de narrativa os fatos ocorridos em uma determinada reunião.<br />
            <br />
            Na atividade notarial, ela representa a função primordial do notário, que é a de
            materializar em narrativa aquilo que viu e ouviu por seus próprios sentidos.<br />
            <br />
            Esta competência está descrita no art. 6º, inciso III da Lei 8.935/94 que dispõe:<br />
            <br />
            <i>Art 6º Aos notários compete:<br />
                omissis...<br />
                <span style="margin-left: 30px;">III - autenticar fatos.</span></i><br />
            <br />
            Desta forma, o notário requisitado para lavrar uma ata notarial utiliza apenas seus
            sentidos e atua como um mero espectador do fatos. E ao narrá-los com suas palavras,
            não deve emitir qualquer juízo de valor.<br />
            <br />
            Os fatos narrados em uma ata notarial possuem presunção de autenticidade, pois portam
            a fé pública do notário, e podem ser apresentados como meio de prova em um processo
            judicial com uma certidão expedida pelo tabelionato.<br />
            <br />
            A busca por este serviço tem aumentado sensivelmente nos tabelionatos de notas,
            haja vista sua praticidade e confiabilidade perante a Justiça.<br />
            <br />
            Desta forma, o notário requisitado para lavrar uma ata notarial utiliza apenas seus
            sentidos e atua como um mero espectador do fatos. E ao narrá-los com suas palavras,
            não deve emitir qualquer juízo de valor.<br />
            <br />
            Os fatos narrados em uma ata notarial possuem presunção de autenticidade, pois portam
            a fé pública do notário, e podem ser apresentados como meio de prova em um processo
            judicial com uma certidão expedida pelo tabelionato.<br />
            <br />
            A busca por este serviço tem aumentado sensivelmente nos tabelionatos de notas,
            haja vista sua praticidade e confiabilidade perante a Justiça.<br />
            <br />
            <strong>Aplicações</strong><br />
            Qualquer fato pode ser registrado em uma ata notarial, desde que licitamente reproduzido,
            como por exemplo:<br />
            <br />
            <span style="margin-left: 30px;">&#9679; o levantamento dos bens que guarnecem uma residência,
                uma fazenda, ou uma empresa;</span><br />
            <span style="margin-left: 30px;">&#9679; as decisões tomadas em reuniões e assembléias;</span><br />
            <span style="margin-left: 30px;">&#9679; o teor de documentos (páginas) publicados na
                internet;</span><br />
            <span style="margin-left: 30px;">&#9679; o conteúdo de um cofre no momento de sua abertura;</span><br />
            <span style="margin-left: 30px;">&#9679; a descrição do estado de conservação de determinado
                bem;</span><br />
            <span style="margin-left: 30px;">&#9679; um acidente de transito;</span><br />
            <span style="margin-left: 30px;">&#9679; dentre outras diversas</span><br />
            <br />
            <strong>Vantagens ao usar ata notarial</strong><br />
            Registro perpétuo do fato: O fato é transcrito na ata, que permanece registrada
            em livros do tabelionato, permitindo que seu conteúdo seja futuramente recuperado
            por certidões, podendo ser utilizada como meio de prova em um processo judicial.<br />
            <br />
            Presunção de autenticidade: Por meio da fé pública do notário, ao lavrar a narrativa
            de um fato juridicamente relevante, está conferindo-lhe a presunção de autenticidade.
            Em outras palavras, eleva o registro deste fato à categoria de <strong>poderoso meio
                de prova</strong>.<br />
            <br />
            <strong>Procedimento</strong><br />
            Para ter um fato registrado em ata notarial, é necessário agendar com o notário
            antecipadamente, a fim de garantir sua presença na ocorrência do fato, que é imprescindível.<br />
            <br />
            Nas atas de comprovação de páginas (sites) na Rede Mundial de Computadores, basta
            dirigir-se ao Tabelionato de Notas.<br />
            <br />
            Resumo:<br />
            Ata notarial - É o instrumento público pelo qual o tabelião de notas constata a
            ocorrência de fatos objetivamente, perpetuando a notícia de sua existência e circunstâncias,
            tendo sido usada com freqüência na pré-constituição de provas.<br />
            <br />
            <strong>Custo</strong><br />
            O custo de uma ata notarial pode variar conforme a diligência necessária (deslocamento
            e tempo do tabelião para comparecer ao momento do fato).<br />
            <br />
            As atas que não requerem diligência, tais como as certidões expedidas via internet
            e conteúdo de sites, podem ser feitas diretamente no tabelionato, e são bastante
            acessíveis.
            <br />
            <br />
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="#topo" rel="nofollow"
                Style="float: right;"><strong>voltar ao topo &raquo;</strong></asp:HyperLink>
            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/contato.aspx" rel="nofollow"><strong>&laquo; Solicite um contato</strong></asp:HyperLink>
            <br />
        </p>
    </div>
    <div class="pg_servicos">
        <a name="testamento"></a>
        <h2>
            Testamento</h2>
        <p>
            Na definição de Ubaldino de Azevedo, "Entende-se por testamento público aquele que
            é escrito pelo oficial público, em seu livro de notas, mediante o ditado, ou declarações
            do testador, expressas na língua nacional, na presença de cinco testemunhas (hoje
            duas, conforme art.1.864, inciso II do Código Civil de 2002), que assistam a todo
            o ato; e que depois de escrito, seja lido pelo oficial, na presença do testador
            e das testemunhas, ou pelo testador, se o quiser, na presença destas e do oficial,
            tudo assinado, o testador, as testemunhas e o oficial. São esses os requisitos reclamados
            para a perfeição do testamento público. E porque assuma a forma de escritura pública,
            é bem de ver-se que as formalidades exigidas para as escrituras devam ser observadas
            nos testamentos.<br />
            <br />
            O testamento público é a forma mais expedita e segura de testar. Escrito em livros
            de notas, pelo oficial, fica o instrumento a salvo de qualquer extravio; tem seu
            conteúdo conferido por funcionário especializado, que o forrará de disposições írritas,
            ou ilegais no dizer de Ubaldino de Azevedo.
            <br />
            <br />
            <asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="#topo" rel="nofollow"
                Style="float: right;"><strong>voltar ao topo &raquo;</strong></asp:HyperLink>
            <asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="~/contato.aspx" rel="nofollow"><strong>&laquo; Solicite um contato</strong></asp:HyperLink>
            <br />
        </p>
    </div>
    <div class="pg_servicos">
        <a name="haia"></a>
        <h2>
            Apostilamento de Haia Documentos Estrangeiros</h2>
        <p>
            <br />
            <br />
            <asp:HyperLink ID="HyperLink14" runat="server" NavigateUrl="#topo" rel="nofollow"
                Style="float: right;"><strong>voltar ao topo &raquo;</strong></asp:HyperLink>
            <asp:HyperLink ID="HyperLink15" runat="server" NavigateUrl="~/contato.aspx" rel="nofollow"><strong>&laquo; Solicite um contato</strong></asp:HyperLink>
            <br />
        </p>
    </div>
</asp:Content>
