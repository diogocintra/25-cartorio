﻿Imports System.IO
Imports System.Net.Mail
Imports System.Web.Configuration
Imports System.Data
Partial Public Class contato
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        'Encode para brasileiro
        Dim encode As System.Text.Encoding
        encode = System.Text.Encoding.GetEncoding("iso-8859-1")

        'Objeto do Email
        Dim email As New MailMessage

        'Le o email criado
        Dim objReader As New StreamReader(ConfigurationManager.AppSettings("bsRoot") & "contato.html", encode)

        'Le o arquivo até o fim
        Dim corpo As String = objReader.ReadToEnd()
        corpo = Replace(corpo, "[#nome#]", Me.txNome.Text)
        corpo = Replace(corpo, "[#email#]", Me.txEmail.Text)
        corpo = Replace(corpo, "[#telefone#]", Me.txDDD.Text & "-" & Me.txTelefone.Text)
        corpo = Replace(corpo, "[#assunto#]", Me.txAssunto.Text)
        corpo = Replace(corpo, "[#mensagem#]", Me.txMensagem.Text)
        corpo = Replace(corpo, "[#assinatura#]", ConfigurationManager.AppSettings("Assinatura"))
        corpo = Replace(corpo, "[#data#]", FormatDateTime(Now, DateFormat.LongDate))

        'Fecha e destroi a conexão
        objReader.Close()
        objReader = Nothing


        ' cria uma instância do objeto MailMessage
        Dim mMailMessage As New MailMessage()

        ' Define o endereço do remetente contato@25serviconotarial.com.br
        mMailMessage.From = New MailAddress("site@25serviconotarial.com.br", "25º Tabelião de Notas Millani")

        ' Define o destinario da mensagem
        mMailMessage.To.Add(New MailAddress("contato@25serviconotarial.com.br", "25º Tabelião de Notas Millani"))

        ' Define o assunto 
        mMailMessage.Subject = "Contato pelo site (" & Me.txNome.Text & ")"

        ' Define o corpo da mensagem
        mMailMessage.Body = corpo

        ' Define o formato do email como HTML
        mMailMessage.IsBodyHtml = True

        ' Define a prioridade da mensagem como normal
        mMailMessage.Priority = MailPriority.Normal

        'Obtain the Network Credentials from the mailSettings section
        Dim credential As New System.Net.NetworkCredential("site", "acesso")

        'Create the SMTP Client
        Dim client As New SmtpClient()
        client.Host = ConfigurationManager.AppSettings("bsSmtp")
        client.Credentials = credential

        'Envia Email por SMTP
        client.Send(mMailMessage)

        Response.Redirect("contato-agradecimento.aspx")
    End Sub
End Class