﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/master/Principal.Master"
    CodeBehind="Default.aspx.vb" Inherits="CartorioMarcioMilani2010._Default1" %>

<%@ Register Src="controls/menu_servicos.ascx" TagName="menu_servicos" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="O 25º Tabelião de Notas Milani(cartório) fica localizado na Lapa, na grande São Paulo, e destina-se à pratica de certos atos jurídicos, tais como escrituras, procurações, atas notariais, testamentos, reconhecimentos de firmas (assinaturas), autenticação de documentos, entre outros." />
    <meta name="keywords" content="cartorios,cartórios,cartório,cartorio,tabelião,tabelionato,tabelionatos,25º Tabelião de Notas Milani,autenticação,escritura pública,inventário,partilha,procurações,separação e divórcio,testamento" />
    <link rel="canonical" href="http://www.25serviconotarial.com.br/" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:menu_servicos ID="menu_servicos1" runat="server" />
    <h1>
        Bem vindo ao 25º Tabelião de Notas Milani</h1>
    <p>
        Tabelionato, o antigo Cartório, é um estabelecimento oficial dirigido por um tabelião
        de notas ou de protestos, e destina-se à pratica de certos atos jurídicos, tais
        como escrituras, procurações, atas notariais, testamentos, reconhecimentos de firmas
        (assinaturas), autenticação de documentos, entre outros.<br />
        <br />
        Os cartórios se dividem em ofícios judiciais, que cuidam do protocolo das varas
        judiciárias das comarcas, e os ofícios extrajudiciais que tem especialidades distintas,
        como o tabelionato de notas ou de protesto, o registro civil, o registro imobiliário
        e o registro de títulos e documentos, que prestam uma variedade de serviços públicos
        à população.
    </p>
    <div style="margin: 10px 0px 10px 0px; text-align: right;">
        <asp:HyperLink ID="HyperLink15" runat="server" NavigateUrl="~/servicos.aspx">Conheça o 25º Tabelião de Notas Milani</asp:HyperLink>
        <asp:HyperLink ID="HyperLink16" runat="server" NavigateUrl="~/servicos.aspx">
            <asp:Image ID="Image10" runat="server" ImageUrl="~/img/bt-padrao.png" Style="margin-right: 20px;" /></asp:HyperLink>
    </div>
    <div id="valores_home">
        <h1 style="margin: 5px 10px 0px 10px;">
            Valores</h1>
        <p style="margin: 0px 10px 0px 10px; line-height: 16px;">
            Os valores cobrados pelos serviços oferecidos são fixados por lei estadual, em função
            da natureza do ato a ser praticado, não podendo variar de cartório para cartório
            nem no próprio cartório.
        </p>
        <div style="margin: 0 10px; text-align: right;">
            <asp:HyperLink ID="HyperLink17" runat="server" NavigateUrl="~/valores/tabela_de_valores_2016.pdf"
                Target="_blank" rel="nofollow">Verifique os valores dos serviços</asp:HyperLink>
            <asp:HyperLink ID="HyperLink18" runat="server" NavigateUrl="~/valores/tabela_de_valores_2016.pdf"
                Target="_blank">
                <asp:Image ID="Image11" runat="server" ImageUrl="~/img/bt-padrao.png" /></asp:HyperLink>
        </div>
    </div>
</asp:Content>
