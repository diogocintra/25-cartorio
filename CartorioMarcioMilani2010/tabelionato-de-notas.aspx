﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/master/Principal.Master"
    CodeBehind="tabelionato-de-notas.aspx.vb" Inherits="CartorioMarcioMilani2010.tabelionato_de_notas"
    Title="Tabelionato de Notas | 25º Tabelião de Notas Millani" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="Tebela de Valores do 25º Tabelião de Notas Milani" />
    <meta name="keywords" content="valores,tabela de valores,notas explicativas,tabelionato de notas" />
    <link rel="canonical" href="http://www.25serviconotarial.com.br/tabelionato-de-notas.aspx" />
    <link href="<%=ConfigurationManager.AppSettings("bsHtml") %>css/tabelionato.css"
        rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>
        Tabelionato de Notas - Notas Explicativas</h1>
    <div class="pg_tabelionato">
        <h2>
            Nota 1</h2>
        <ul class="li_tabelionato">
            <li><span class="lista_pai">1.1.-</span> Nas hipóteses de hipoteca e penhor os emolumentos
                serão calculados sobre o débito confessado ou estimado.
                <ul>
                    <li><span class="lista_filho">1.1.1.-</span> Quando dois ou mais bens forem dados em
                        garantia, para os quais não tenha sido individualmente atribuído o valor, a base
                        de cálculo para cobrança de emolumentos será o valor do negócio jurídico, atribuído
                        ou estimado, dividido pelo número de bens ofertados. </li>
                </ul>
            </li>
            <li><span class="lista_pai">1.2.-</span> Nas hipóteses de locação os emolumentos serão
                calculados sobre a soma dos alugueres, ou, se por prazo indeterminado, sobre o valor
                correspondente a 12 (doze) meses de locação.</li>
            <li><span class="lista_pai">1.3.-</span> No caso de usufruto, os emolumentos serão calculados
                sobre a terça parte do valor do imóvel, observado o disposto no item 1 da tabela.</li>
            <li><span class="lista_pai">1.4.-</span> Na enfiteuse, a base de cálculo dos emolumentos
                será de 20% (vinte por cento) sobre o valor do imóvel, em se tratando de domínio
                direto e de 80% (oitenta por cento) no caso de domínio útil, observado o disposto
                no item 1 da tabela e artigo 7.º desta lei.</li>
            <li><span class="lista_pai">1.5.-</span> No caso de instituição de servidão os emolumentos
                terão como base 20% ( vinte por cento) do valor do imóvel, respeitando-se o mínimo
                previsto no item 1 da tabela, combinado com o artigo 7.º desta lei.</li>
            <li><span class="lista_pai">1.6.-</span> As transações, cuja instrumentalização admitem
                forma particular, terão o valor previsto no item 1 da tabela reduzido em 40% (quarenta
                por cento), devendo sempre ser respeitado o mínimo ali previsto, combinado com o
                artigo 7.º desta lei.</li>
            <li><span class="lista_pai">1.7.-</span> Quando o imóvel objeto da escritura for apartamento
                e garagens, será considerado um único imóvel para fins de cobrança.
                <ul>
                    <li><span class="lista_filho">1.7.1-</span> Será também considerado como único, o imóvel
                        rural ou terreno urbano que, embora tenha mais de uma matrícula, tenha lançamento
                        tributário por apenas um número de contribuinte.</li>
                </ul>
            </li>
            <li>
                <asp:HyperLink ID="HyperLink17" runat="server" NavigateUrl="~/valores/tabela_de_valores_2016.pdf"
                    Target="_blank" rel="nofollow" Enabled="True">Verifique os valores dos serviços</asp:HyperLink>
            </li>
        </ul>
    </div>
    <div class="pg_tabelionato">
        <h2>
            Nota 2 - Condições especiais de emolumentos</h2>
        <ul class="li_tabelionato">
            <li><span class="lista_pai">2.1.-</span> Nas escrituras de compromisso de venda e compra,
                os emolumentos serão de 50% (cinqüenta por cento) do valor das escrituras com valor
                declarado.</li>
            <li><span class="lista_pai">2.2.-</span> Nas escrituras de quitação, o valor dos emolumentos
                será de 1/5 (um quinto) do valor fixado para as escrituras com valor declarado.</li>
            <li><span class="lista_pai">2.3.-</span> Nas escrituras de emissão de debêntures, o
                valor dos emolumentos será de 50% (cinqüenta por cento) do valor previsto no item
                1 da tabela.</li>
            <li><span class="lista_pai">2.4.-</span> Nas escrituras de instituição e especificação
                de condomínio, cuja incorporação tenha sido instrumentada por ato público, cobrar-se-á
                50% (cinqüenta por cento) do valor previsto no item 1 da tabela.</li>
            <li><span class="lista_pai">2.5.-</span> Loteamentos regularizados ou registrados -
                Os emolumentos corresponderão a 50% (cinqüenta por cento) do valor previsto no item
                1 da tabela, respeitado o mínimo ali previsto, pelos atos relativos a:
                <ul>
                    <li><span class="lista_filho">a-</span> Cumprimento de contratos particulares de compromisso
                        de venda e compra oriundos de loteamentos regularizados pelas Prefeituras Municipais,
                        de conformidade com o artigo 40 e seguintes da Lei Federal n. 6.766, de 19 de dezembro
                        de 1.979;</li>
                    <li><span class="lista_filho">b-</span> Cumprimento de contratos de compromisso de venda
                        e compra, não quitados, de lotes isolados de loteamentos registrados, desde que
                        o seu valor não seja superior a 500 (quinhentas) UFESP's e sua área não ultrapasse
                        300 (trezentos) metros quadrados.</li>
                </ul>
            </li>
            <li><span class="lista_pai">2.6.-</span> Imóveis financiados por entidade financeira:
                <ul>
                    <li><span class="lista_filho">a-</span> os emolumentos serão calculados pela tabela
                        de escritura com valor declarado, aplicando-se redução de 20% (vinte por cento);</li>
                    <li><span class="lista_filho">b-</span> mesmo que a escritura contenha outros atos acessórios
                        será cobrado apenas um ato, o de maior valor, não se aplicando neste caso a regra
                        da nota 4.3.;</li>
                    <li><span class="lista_filho">c-</span> no caso de prédio acabado, a base de cálculo
                        será o valor total do prédio;</li>
                    <li><span class="lista_filho">d-</span> no caso de aquisição de terreno com financiamento
                        de prédio a ser construído, a base de cálculo será a soma do valor do terreno mais
                        o financiamento para construção;</li>
                    <li><span class="lista_filho">e-</span> estes critérios se aplicam nos seguintes casos:
                        <ul>
                            <li><span class="lista_neto">I-</span> aquisição imobiliária para fins residenciais,
                                feita através de Consórcios ou financiada pelo Sistema Financeiro da Habitação ou
                                qualquer outra entidade financeira fiscalizada pelo Banco Central do Brasil;</li>
                            <li><span class="lista_neto">II-</span> aquisição imobiliária para fins residenciais
                                financiada pelo Governo do Estado e pelas Prefeituras Municipais, diretamente ou
                                através de suas companhias habitacionais.</li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><span class="lista_pai">2.7-</span> Os testamentos públicos que versarem sobre patrimônio
                com valor não superior a 3.000 U-FESP's, terão seus emolumentos reduzidos em 50%
                (cinqüenta por cento).</li>
            <li><span class="lista_pai">Lei 13.290 de 22 de dezembro de 2008.</span>
                <ul>
                    <li>Artigo 6º - Nos atos que envolvam a aquisição do terreno pelo empreendedor, retificação,
                        registro de parcelamento do solo, incorporação, averbação da construção, instituição
                        de condomínio ou parcelamento do solo, relativos a empreendimentos de interesse
                        social promovidos pela CDHU ou COHAB, empresa pública, sociedade de economia mista,
                        ou promovido por cooperativa habitacional ou associação de moradores, serão as custas
                        e emolumentos dos oficiais de registro de imóveis e dos notários reduzidos em 75%
                        (setenta e cinco por cento).</li>
                    <li>Artigo 7º - Nos atos que envolvam a aquisição do terreno pelo empreendedor, retificação,
                        registro de parcelamento de solo, incorporação, averbação da construção, instituição
                        de condomínio ou parcelamento do solo, relativos a empreendimentos de interesse
                        social localizado em Zona Especial de Interesse Social - ZEIS, ou de outra forma
                        definido pelo Município como de interesse social, serão as custas e emolumentos
                        do Registro de Imóveis e do Tabelião de Notas reduzidos em 50% (cinqüenta por cento).</li>
                </ul>
            </li>
            <li>
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/valores/tabela_de_valores_2016.pdf"
                    Target="_blank" rel="nofollow" Enabled="true">Verifique os valores dos serviços</asp:HyperLink>
            </li>
        </ul>
    </div>
    <div class="pg_tabelionato">
        <h2>
            Nota 3 - Vários bens, direitos ou atos na mesma escritura</h2>
        <ul class="li_tabelionato">
            <li><span class="lista_pai">3.1.-</span> Nas escrituras de transmissão, oneração ou
                de atribuição de direitos reais, os emolumentos serão calculados levando-se em conta
                o valor de cada uma das unidades imobiliárias ou de direitos transacionados, observadas
                as bases previstas no artigo 7.º desta lei
                <ul>
                    <li><span class="lista_filho">3.1.1.-</span> Nas escrituras de permuta, ou de divisão
                        de imóvel, ou de partilha, o cálculo deverá ser feito por pagamento, obedecendo
                        os critérios dispostos nesta lei, quando ao interessado for atribuído mais de um
                        bem ou direito, salvo disposição em contrário aqui prevista.</li>
                </ul>
            </li>
            <li><span class="lista_pai">3.2.-</span> As escrituras de venda e compra e cessão consubstanciam
                dois negócios jurídicos, devendo o cedente e o adquirente pagar as despesas integrais
                de cada negócio.</li>
            <li><span class="lista_pai">3.3.-</span> Se a escritura contiver, além do ato jurídico
                principal, outros que lhe forem acessórios, entre as mesmas partes ou não, os emolumentos
                serão calculados sobre o negócio jurídico de maior valor, com o acréscimo de 1/4
                (um quarto) de cada um dos demais, respeitando o mínimo previsto no item 1 da tabela,
                combinado com o disposto no artigo 7.º desta lei.</li>
            <li><span class="lista_pai">3.4.-</span> As escrituras de venda e compra, com mútuo
                e outorga de garantia, serão cobradas como um ato principal e dois acessórios.</li>
            <li><span class="lista_pai">3.5.-</span> A reserva do usufruto deve ser tida como ato
                acessório, devendo seus emolumentos ter a redução tratada no item 3.3, destas Notas
                Explicativas.</li>
            <li><span class="lista_pai">3.6.-</span> Quando em qualquer escritura houver outorga
                de procuração e/ou substabelecimento, também serão devidos emolumentos sobre a prática
                desses atos.</li>
            <li><span class="lista_pai">3.7.-</span> As intervenções ou anuências de terceiros não
                autorizam acréscimos de preço, a não ser que impliquem outros atos.</li>
            <li>
                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/valores/tabela_de_valores_2016.pdf"
                    Target="_blank" rel="nofollow" Enabled="True">Verifique os valores dos serviços</asp:HyperLink>
            </li>
        </ul>
    </div>
    <div class="pg_tabelionato">
        <h2>
            Nota 4 - Traslado</h2>
        <ul class="li_tabelionato">
            <li><span class="lista_pai">4.1.-</span> No preço das escrituras se compreende o primeiro
                traslado, devendo os demais ser cobrados observando-se o item 5 da tabela.</li>
            <li>
                <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/valores/tabela_de_valores_2016.pdf"
                    Target="_blank" rel="nofollow" Enabled="True">Verifique os valores dos serviços</asp:HyperLink>
            </li>
        </ul>
    </div>
    <div class="pg_tabelionato">
        <h2>
            Nota 5 - Transcrição de documentos</h2>
        <ul class="li_tabelionato">
            <li><span class="lista_pai">5.1.-</span> Nenhum acréscimo será devido pela transcrição,
                nos atos notariais, de alvarás, mandados, guias de recolhimento de tributos, certidões
                em geral e outros documentos, nem pelo arquivamento de procuração ou de qualquer
                documento necessário à pratica do ato.</li>
            <li>
                <asp:HyperLink ID="HyperLink7" runat="server" NavigateUrl="~/valores/tabela_de_valores_2016.pdf"
                    Target="_blank" rel="nofollow" Enabled="True">Verifique os valores dos serviços</asp:HyperLink>
            </li>
        </ul>
    </div>
    <div class="pg_tabelionato">
        <h2>
            Nota 6 - Escritura de incorporação e/ou de especificação de condomínio</h2>
        <ul class="li_tabelionato">
            <li><span class="lista_pai">6.1.-</span> A base de cálculo do preço das escrituras de
                incorporação e/ou de especificação de condomínio será obtida da seguinte forma:
                <ul>
                    <li><span class="lista_filho">a-</span> a base de cálculo será o valor que resultar
                        da soma do valor do terreno com o da avaliação do custo global da obra ou construção,
                        apresentada pelo incorporador.</li>
                    <li><span class="lista_filho">b-</span> a avaliação de que trata a alínea "a" deve ser
                        elaborada com base nos valores de metro quadrado fornecidos pelos Sindicatos da
                        Construção Civil e constantes de revistas especializadas para o tipo de prédio objeto
                        da incorporação, se outro maior não for declarado.</li>
                    <li><span class="lista_filho">c-</span> havendo, porém, atribuição de unidades, será
                        acrescido ao valor da escritura, 1/3 (um terço) dos emolumentos calculado pelo valor
                        de cada unidade, não se aplicando, no caso, o previsto no subitem 3.1 destas Notas
                        Explicativas. Considera-se, para esse fim, a(s) unidade(s) e respectiva(s) vaga(s)
                        de garagem.</li>
                </ul>
            </li>
            <li>
                <asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="~/valores/tabela_de_valores_2016.pdf"
                    Target="_blank" rel="nofollow" Enabled="True">Verifique os valores dos serviços</asp:HyperLink>
            </li>
        </ul>
    </div>
    <div class="pg_tabelionato">
        <h2>
            Nota 7 - Procurações</h2>
        <ul class="li_tabelionato">
            <li><span class="lista_pai">7.1.-</span> Quando em um mesmo instrumento, além da procuração,
                contiver a formalização de substabelecimento ou revogação, os valores de emolumentos
                serão calculados por inteiro e por ato.</li>
            <li>
                <asp:HyperLink ID="HyperLink11" runat="server" NavigateUrl="~/valores/tabela_de_valores_2016.pdf"
                    Target="_blank" rel="nofollow" Enabled="True">Verifique os valores dos serviços</asp:HyperLink>
                
            </li>
        </ul>
    </div>
    <div class="pg_tabelionato">
        <h2>
            Nota 8 - Acréscimo por atos praticados fora do horário normal ou fora do tabelionato</h2>
        <ul class="li_tabelionato">
            <li><span class="lista_pai">8.1.-</span> Nos atos sem valor declarado, lavrados fora
                do horário normal ou fora do tabelionato, exceto quando do interesse dos órgãos
                públicos em geral, os emolumentos serão cobrados em dobro, fa-zendo o tabelião circunstanciada
                menção na escritura, sem prejuízo do reembolso das despesas com condução.</li>
            <li>
                <asp:HyperLink ID="HyperLink13" runat="server" NavigateUrl="~/valores/tabela_de_valores_2016.pdf"
                    Target="_blank" rel="nofollow" Enabled="True">Verifique os valores dos serviços</asp:HyperLink>
                
            </li>
        </ul>
    </div>
    <div class="pg_tabelionato">
        <h2>
            Nota 9 - Atos declarados incompletos ou sem efeito</h2>
        <ul class="li_tabelionato">
            <li><span class="lista_pai">9.1.-</span> Pelo ato notarial declarado incompleto, por
                falta de assinatura, por culpa ou a pedido de qual-quer das partes, será devido
                1/3 (um terço) dos emolumentos. Se não for consignado o motivo, o Escrevente e o
                Tabelião, responderão solidariamente pela terça parte das parcelas previstas no
                arti-go 19, inciso I, letras "b", "c" e "d", desta lei.</li>
            <li><span class="lista_pai">9.2.-</span> Pelo ato notarial declarado sem efeito por
                erro de redação ou impressão e se nenhuma das partes o houver assinado, nada será
                devido.</li>
            <li><span class="lista_pai">9.3.-</span> É proibida a cobrança de qualquer valor em
                decorrência da prática de ato de retificação, ou que teve de ser refeito ou renovado,
                em razão de erro imputável ao respectivo Tabelião.</li>
            <li>
                <asp:HyperLink ID="HyperLink15" runat="server" NavigateUrl="~/valores/tabela_de_valores_2016.pdf"
                    Target="_blank" rel="nofollow" Enabled="True">Verifique os valores dos serviços</asp:HyperLink>
                
            </li>
        </ul>
    </div>
    <div class="pg_tabelionato">
        <h2>
            Nota 10 - Autenticação de cópias reprográficas</h2>
        <ul class="li_tabelionato">
            <li><span class="lista_pai">10.1.-</span> A cada página de documento copiada corresponderá
                uma autenticação, a qual poderá ser aposta no anverso ou verso do documento, devendo,
                na face que não recebeu a certificação, ser lançado o carimbo personalizado da serventia
                mencionando essa circunstância, vedada, expressamente, a autenticação em face do
                documento desprovida de quaisquer caracteres gráficos.</li>
            <li><span class="lista_pai">10.2.-</span> Apenas um ato de autenticação será feito para
                a frente e o verso do CIC, do Título de Eleitor ou de Cédula de Identidade ou qualquer
                outra cédula que identifique o usuário.</li>
            <li><span class="lista_pai">10.3.-</span> Quando a cópia reprográfica for extraída em
                máquina própria da serventia, o Notário repassará o custo operacional à parte, até
                o máximo de 0,026 UFESP's. Se, entretanto, extraída em papel próprio da serventia
                que contenha requisitos de segurança, cobrar-se-á até, no máximo, 0,05 U-FESP's.
                Neste caso, tal cópia deverá, necessariamente, ser autenticada de forma regular
                pelo Notário.</li>
            <li>
                <asp:HyperLink ID="HyperLink19" runat="server" NavigateUrl="~/valores/tabela_de_valores_2016.pdf"
                    Target="_blank" rel="nofollow" Enabled="True">Verifique os valores dos serviços</asp:HyperLink>
                
            </li>
        </ul>
    </div>
    <div class="pg_tabelionato">
        <h2>
            Nota 11 - Despesas de serviços extra-notariais</h2>
        <ul class="li_tabelionato">
            <li><span class="lista_pai">11.1.-</span> O notário que se incumbir da prestação de
                serviços que não são de sua competência exclusiva e nem de sua obrigação, mas necessários
                ao aperfeiçoamento do ato, cobrará as despesas efetuadas e custas efetivas, desde
                que autorizado pela parte interessada.</li>
            <li>
                <asp:HyperLink ID="HyperLink21" runat="server" NavigateUrl="~/valores/tabela_de_valores_2016.pdf"
                    Target="_blank" rel="nofollow" Enabled="True">Verifique os valores dos serviços</asp:HyperLink>
                
            </li>
        </ul>
    </div>
    <div class="pg_tabelionato">
        <h2>
            Nota 12 - Central de testamentos</h2>
        <ul class="li_tabelionato">
            <li><span class="lista_pai">12.1-</span> Toda escritura de testamento tratada no item
                8 da tabela deverá ser comunicada à Central de Testamentos, prevista no Provimento
                06/94, da Egrégia Corregedoria-Geral da Justiça deste Estado, devendo o Tabelião
                a ela remeter, até o 5.º dia útil depois de sua lavratura, o valor correspondente
                a R$ 38,30 (trinta e oito reais e trinta centavos), por escritura, que equivale
                ao determinado no item 5 da tabela, referente a atos de certidão ou traslado ou
                pública forma.
                <ul>
                    <li><span class="lista_filho">12.1.1-</span> O valor a que se refere o subitem acima
                        será deduzido da parte tida na respectiva tabela como receita do Notário.</li>
                </ul>
            </li>
            <li><span class="lista_pai">12.2-</span> As informações a serem prestadas pela referida
                Central de Testamentos terão um custo unitário equivalente ao valor previsto no
                item 12.1 destas Notas Explicativas.</li>
            <li>
                <asp:HyperLink ID="HyperLink23" runat="server" NavigateUrl="~/valores/tabela_de_valores_2016.pdf"
                    Target="_blank" rel="nofollow" Enabled="True">Verifique os valores dos serviços</asp:HyperLink>
                
            </li>
        </ul>
    </div>
    <div class="pg_tabelionato">
        <h2>
            Nota 13
        </h2>
        <p>
            A Contribuição de solidariedade, instituída pela Lei n. 11.021, de 28 de dezembro
            de 2001, tem, como base de cálculo, o valor destinado ao Tabelião.<br />
            <strong>Lei n. 11.331, de 26 de dezembro de 2002.</strong></p>
        <ul class="li_tabelionato">
            <li><span class="lista_pai">Artigo 7.º-</span> O valor da base de cálculo a ser considerado
                para fins de enquadramento nas tabelas de que trata o artigo 4.º, relativamente
                aos atos classificados na alínea "b" do inciso III do artigo 5.º, ambos desta lei,
                será determinado pelos parâmetros a seguir, prevalecendo o que for maior:
                <ul>
                    <li><span class="lista_filho">I-</span> preço ou valor econômico da transação ou do
                        negócio jurídico declarado pelas partes;</li>
                    <li><span class="lista_filho">II-</span> valor tributário do imóvel estabelecido no
                        último lançamento efetuado pela Prefeitura Municipal, para efeito de cobrança de
                        imposto sobre a propriedade predial e territorial urbana, ou o valor da avaliação
                        do imóvel rural aceito pelo órgão federal competente, considerando o valor da terra
                        nua, as acessões e as benfeitorias; </li>
                    <li><span class="lista_filho">III -</span> base de cálculo utilizada para o recolhimento
                        do imposto de transmissão "inter vivos" de bens imóveis.</li>
                </ul>
            </li>
            <li><span class="lista_pai">Parágrafo único:</span> Nos casos em que, por força de lei,
                devam ser utilizados valores decorrentes de avaliação judicial ou fiscal, estes
                serão os valores considerados para os fins do disposto na alínea "b" do inciso III
                do artigo 5.º desta lei.</li>
            <li><span class="lista_pai">Artigo 8.º-</span> A União, os Estados, o Distrito Federal,
                os Municípios e as respectivas autarquias, são isentos do pagamento das parcelas
                dos emolumentos destinadas ao Estado, à Carteira de Previdência das Serventias Não
                Oficializadas da Justiça do Estado, ao custeio dos atos gratuitos de registro civil
                e ao Fundo Especial de Despesa do Tribunal de Justiça. Parágrafo único - O Estado
                de São Paulo e suas respectivas autarquias são isentos do pagamento de emolumentos
            </li>
            <li><span class="lista_pai">Artigo 9.º-</span> São gratuitos:
                <ul>
                    <li><span class="lista_filho">I-</span> os atos previstos em lei;</li>
                    <li><span class="lista_filho">II-</span> os atos praticados em cumprimento de mandados
                        judiciais expedidos em favor da parte beneficiária da justiça gratuita, sempre que
                        assim for expressamente determinado pelo Juízo.</li>
                </ul>
            </li>
            <li><span class="lista_pai">Artigo 10.º-</span> Na falta de previsão nas notas explicativas
                e respectivas tabelas, somente poderão ser cobradas as despesas pertinentes ao ato
                praticado, quando autorizadas pela Corregedoria-Geral da Justiça.</li>
            <li><span class="lista_pai">Artigo 13.º-</span> Salvo disposição em contrário, os notários
                e os registradores poderão exigir depósito prévio dos valores relativos aos emolumentos
                e das despesas pertinentes ao ato, fornecendo aos interessados, obrigatoriamente,
                recibo com especificação de todos valores.</li>
            <li><span class="lista_pai">Artigo 14.º-</span> Os notários e os registradores darão
                recibo dos valores cobrados, sem prejuízo da indicação definitiva e obrigatória
                dos respectivos emolumentos à margem do documento entregue ao interessado.</li>
            <li><span class="lista_pai">Artigo 30.º-</span> Contra a cobrança, a maior ou a menor,
                de emolumentos e despesas devidas, poderá qualquer interessado reclamar, por petição,
                ao Juiz Corregedor-Permanente.</li>
            <li><span class="lista_pai">Artigo 32.º-</span> Sem prejuízo da responsabilidade disciplinar,
                os notários, os registradores e seus prepostos estão sujeitos à pena de multa de,
                no mínimo, 100 (cem) e, no máximo, 500 (quinhentas) U-FESP's, ou outro fator que
                a substituir, nas hipóteses de:
                <ul>
                    <li><span class="lista_filho">I-</span> recebimento de valores não previstos ou maiores
                        que os previstos nas tabelas, nos casos em que não caiba a aplicação do inciso I
                        do artigo 34 desta lei;</li>
                    <li><span class="lista_filho">II-</span> descumprimento das demais disposições desta
                        lei</li>
                </ul>
            </li>
            <li><span class="lista_pai">§ 3.º-</span> Na hipótese de recebimento de importâncias
                indevidas ou excessivas, além da pena de multa, o infrator fica obrigado a restituir
                ao interessado o décuplo da quantia irregularmente cobrada.</li>
            <li><span class="lista_pai">Artigo 37.º-</span> Sempre que forem alteradas ou divulgadas
                novas tabelas, estas não se aplicarão aos atos notariais e de registros já solicitados,
                quando tenha havido ou não depósito total ou parcial dos emolumentos previstos,
                salvo nas hipóteses previstas nas respectivas notas explicativas das tabelas.</li>
            <li>
                <asp:HyperLink ID="HyperLink25" runat="server" NavigateUrl="~/valores/tabela_de_valores_2016.pdf"
                    Target="_blank" rel="nofollow" Enabled="True">Verifique os valores dos serviços</asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
