﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="menu_hr.ascx.vb" Inherits="CartorioMarcioMilani2010.menu_hr" %>
<div id="menu_hr">
    <ul>
        <li>
            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Default.aspx" title="Ir para Página Inicial">Página Inicial</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/servicos.aspx" title="Nossos Serviços">Serviços</asp:HyperLink></li>
        
        <li>
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/tabelionato-de-notas.aspx"
                title="Tabelionato de Notas">Notas Explicativas</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="~/contato.aspx" title="Entre em contato conosco"
                Style="border: 0;">Contato</asp:HyperLink></li>
    </ul>
</div>
<div id="banner">
    <asp:Image ID="Image2" runat="server" ImageUrl="~/img/banner-principal.png" Width="1000px"
        Height="258px" />
</div>
