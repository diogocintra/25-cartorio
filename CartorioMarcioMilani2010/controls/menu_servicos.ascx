﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="menu_servicos.ascx.vb"
    Inherits="CartorioMarcioMilani2010.menu_servicos" %>
<div id="mn_servicos">
    <h2>
        Nossos Serviços</h2>
    <ul>
        <li>
            <asp:Image ID="Image4" runat="server" ImageUrl="~/img/list-mn-servicos.gif" />&nbsp;
            <asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="~/servicos.aspx#escrituras" rel="nofollow">Escrituras</asp:HyperLink>
        </li>
        <li>
            <asp:Image ID="Image5" runat="server" ImageUrl="~/img/list-mn-servicos.gif" />&nbsp;
            <asp:HyperLink ID="HyperLink10" runat="server" NavigateUrl="~/servicos.aspx#procuracoes" rel="nofollow">Procurações</asp:HyperLink>
        </li>
        <li>
            <asp:Image ID="Image6" runat="server" ImageUrl="~/img/list-mn-servicos.gif" />&nbsp;
            <asp:HyperLink ID="HyperLink11" runat="server" NavigateUrl="~/servicos.aspx#reconhecimentos" rel="nofollow">Autenticações e reconhecimentos de firma</asp:HyperLink>
        </li>
        <li>
            <asp:Image ID="Image7" runat="server" ImageUrl="~/img/list-mn-servicos.gif" />&nbsp;
            <asp:HyperLink ID="HyperLink12" runat="server" NavigateUrl="~/servicos.aspx#separacao" rel="nofollow">Divórcio extrajudicial</asp:HyperLink>
        </li>
        <li>
            <asp:Image ID="Image3" runat="server" ImageUrl="~/img/list-mn-servicos.gif" />&nbsp;
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/servicos.aspx#separacao" rel="nofollow">Inventário e partilhas extrajuducial</asp:HyperLink>
        </li>
        <li>
            <asp:Image ID="Image8" runat="server" ImageUrl="~/img/list-mn-servicos.gif" />&nbsp;
            <asp:HyperLink ID="HyperLink13" runat="server" NavigateUrl="~/servicos.aspx#atas-notariais" rel="nofollow">Atas notarias</asp:HyperLink>
        </li>
        <li>
            <asp:Image ID="Image9" runat="server" ImageUrl="~/img/list-mn-servicos.gif" />&nbsp;
            <asp:HyperLink ID="HyperLink14" runat="server" NavigateUrl="~/servicos.aspx#testamento" rel="nofollow">Testamento</asp:HyperLink>
        </li>
        <li>
            <asp:Image ID="Image10" runat="server" ImageUrl="~/img/list-mn-servicos.gif" />&nbsp;
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/servicos.aspx#haia" rel="nofollow">Apostilamento de Haia Documentos Estrangeiros</asp:HyperLink>
        </li>
    </ul>
</div>
