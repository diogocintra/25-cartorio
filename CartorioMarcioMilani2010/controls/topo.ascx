﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="topo.ascx.vb" Inherits="CartorioMarcioMilani2010.topo" %>
<div id="topo">
    <a name="topo" rel="nofollow"></a>
    <p>
        <span style="font-size: 24px;line-height:30px;">25º Tabelião de Notas Milani</span><br />
        <span style="font-size: 18px; color: #E4E4CB;line-height:20px;">Rua Afonso Sardinha, 290 - Lapa São Paulo - SP - CEP 05076-000</span><br />
        <span style="font-size: 18px;">Telefone PABX: 3836-1522</span><br />
        <span style="font-size: 14px; color: #E4E4CB;line-height:35px;">Horário de Funcionamento: De segunda a sexta-feira das 9H às 17H</span>
    </p>
    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx" Title="25º Tabelião de Notas Milani">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/img/logo.png" CssClass="logo" Width="194px" Height="118px" />
    </asp:HyperLink>
</div>
