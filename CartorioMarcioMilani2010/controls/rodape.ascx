﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="rodape.ascx.vb" Inherits="CartorioMarcioMilani2010.rodape" %>
<div id="rodape">
    <p class="copyright">
        25º Tabelião de Notas Milani © Todos os direitos reservados - 2010<br />
        <asp:HyperLink ID="HyperLink25" runat="server" NavigateUrl="http://www.suaempresa.net"
            Target="_blank">Desenvolvido por SuaEmpresa.Net</asp:HyperLink>
    </p>
    <ul>
        <li>
            <asp:HyperLink ID="HyperLink19" runat="server" NavigateUrl="~/Default.aspx" rel="nofollow"
                title="Ir para Página Inicial">Página Inicial</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HyperLink21" runat="server" NavigateUrl="~/servicos.aspx" rel="nofollow"
                title="Nossos Serviços">Serviços</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/tabelionato-de-notas.aspx"
                title="Tabelionato de Notas">Notas Explicativas</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HyperLink24" runat="server" NavigateUrl="~/contato.aspx" rel="nofollow"
                title="Entre em contato conosco">Contato</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/sobre-o-desenvolvedor.aspx"
                title="Sobre o Desenvolvedor" Style="border: 0;">Sobre o Desenvolvedor</asp:HyperLink></li>
    </ul>
</div>
