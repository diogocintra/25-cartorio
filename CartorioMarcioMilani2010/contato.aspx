﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/master/Principal.Master"
    CodeBehind="contato.aspx.vb" Inherits="CartorioMarcioMilani2010.contato" Title="Contato | 25º Tabelião de Notas Millani" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="canonical" href="http://www.25serviconotarial.com.br/contato.aspx" />
    <link href="<%=ConfigurationManager.AppSettings("bsHtml") %>css/contato.css" rel="stylesheet"
        type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>
        Contato</h1>
    <div id="box_contatos">
        <h2>
            Telefones:</h2>
        <p>
            Telefone PABX: 11 3836-1522
        </p>
        <h2>
            E-mail:</h2>
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="mailto:contato@25serviconotarial.com.br"
            Target="_blank">contato@25serviconotarial.com.br</asp:HyperLink>
    </div>
    <div class="pg_servicos">
        <p>
            Entre em contato com o 25º Tabelião de Notas Milani através do nosso formulário
            de contao abaixo.<br />
            Preencha os campos abaixo, lembrando que os itens em <strong>negrito</strong> são
            de preenchimento obrigatório.
        </p>
        <fieldset>
            <label>
                <strong>Nome:</strong>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txNome">*</asp:RequiredFieldValidator>
            </label>
            <br />
            <span>
                <asp:TextBox ID="txNome" runat="server" CssClass="txtbox" Width="500px"></asp:TextBox></span><br />
            <label>
                <strong>E-mail:</strong>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txEmail">*</asp:RequiredFieldValidator>
            </label>
            <br />
            <span>
                <asp:TextBox ID="txEmail" runat="server" CssClass="txtbox" Width="500px"></asp:TextBox></span><br />
            <label>
                <strong>Telefone:</strong>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txDDD">*</asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txTelefone">*</asp:RequiredFieldValidator>
            </label>
            <br />
            <span>
                <asp:TextBox ID="txDDD" runat="server" CssClass="txtbox" Width="20px" MaxLength="2"></asp:TextBox>
                <asp:TextBox ID="txTelefone" runat="server" CssClass="txtbox" Width="80px" MaxLength="9"></asp:TextBox>
            </span>
            <br />
            <label>
                Assunto:</label><br />
            <span>
                <asp:TextBox ID="txAssunto" runat="server" CssClass="txtbox" Width="500px"></asp:TextBox></span><br />
            <label>
                <strong>Mensagem:</strong>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txMensagem">*</asp:RequiredFieldValidator>
            </label>
            <br />
            <span>
                <asp:TextBox ID="txMensagem" runat="server" CssClass="txtbox" Width="500px" Height="80px"
                    TextMode="MultiLine"></asp:TextBox></span><br />
            <span>
                <asp:Button ID="Button1" runat="server" Text="Enviar" class="bt_padrao" /></span>
        </fieldset>
    </div>
</asp:Content>
