﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/master/Principal.Master"
    CodeBehind="sobre-o-desenvolvedor.aspx.vb" Inherits="CartorioMarcioMilani2010.sobre_o_desenvolvedor"
    Title="Sobre a SuaEmpresa.Net | 25º Tabelião de Notas Millani" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="A SuaEmpresa.Net é uma empresa de tecnologia especializada em Publicidade Digital, utiliza as mais modernas tecnologias para a gestão e criação Lojas Virtuais e Websites, campanhas de E-mail Marketing e Links Patrocinados, e Gestão Mídias Sociais." />
    <meta name="keywords" content="SuaEmpresa.Net, tecnologia, publicidade digital, gestão, e-mail marketing, Lojas Virtuais, WebSites, Mídias Sociais, Links Patrocinados, Anúncios Pagos" />
    <link rel="canonical" href="http://www.25serviconotarial.com.br/sobre-o-desenvolvedor.aspx" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>
        A SuaEmpresa.Net</h1>
    <p class="tx_contato">
        A <strong>SuaEmpresa.Net</strong> é uma empresa de tecnologia especializada em Publicidade
        Digital que utiliza as mais modernas tecnologias para a gestão e criação de <a href="http://www.suaempresa.net/lojas-virtuais.aspx"
            title="Gestão, Desenvolvimento e Criação de Lojas Virtuais para Comércio Eletrônico"
            target="_blank">Lojas Virtuais</a> e <a href="http://www.suaempresa.net/website.aspx"
                title="Desenvolvimento e Criação de Websites" target="_blank">Websites</a>
        com as mais modernas técnicas de <a href="http://www.suaempresa.net/marketing-de-busca(seo).aspx"
            title="Otimização de Websites e Lojas Virtuais para Marketing de Busca(SEO)"
            target="_blank">Marketing de Busca(SEO)</a> disponíveis no mercado. Criamos
        e enviamos campanhas de <a href="http://www.suaempresa.net/email-marketing.aspx"
            title="Criação e Envio de Campanhas de E-mail Marketing" target="_blank">E-mail
            Marketing</a>. Gerenciamos e produzimos campanhas de <a href="http://www.suaempresa.net/links-patrocinados.aspx"
                title="Criação e Gestão de Links Patrocinados / Anúncios Pagos no Google AdWords, Bing e Yahoo"
                target="_blank">Links Patrocinados(Anúncios Pagos)</a> e <a href="http://www.suaempresa.net/midia-social.aspx"
                    title="Gestão de Mídias Sociais e Redes de Relacionamento" target="_blank">Mídias
                    Sociais</a>.<br />
        <br />
        Este <strong>Website</strong> foi desenvolvido pela
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.suaempresa.net/"
            title="SuaEmpresa.Net" Target="_blank">SuaEmpresa.Net</asp:HyperLink>
        em HTML Web Standart/Tableless de acordo com as normas técnicas do W3C e desenvolvida
        com todas as técnicas de
        <asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="http://www.suaempresa.net/marketing-de-busca(seo).aspx"
            title="Marketing de Busca(SEO)" Target="_blank">SEO</asp:HyperLink>
        conhecidas no momento para a melhor indexação nos mecanismos de busca e visualização
        nos principais navegadores (Internet Explorer 7, 8 e 9, Google Chrome e Firefox).<br />
        <br />
        Conheça o portfolio e todos os produtos desenvolvidos pela <strong>SuaEmpresa.Net</strong>
        em:
        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="http://www.suaempresa.net/"
            title="SuaEmpresa.Net" Target="_blank">http://www.suaempresa.net/</asp:HyperLink>.
        Teremos imensa satisfação em conhecê-lo.
    </p>
</asp:Content>
