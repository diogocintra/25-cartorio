﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/master/Principal.Master"
    CodeBehind="contato-agradecimento.aspx.vb" Inherits="CartorioMarcioMilani2010.contato_agradecimento"
    Title="Agradecimento | 25º Tabelião de Notas Millani" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="canonical" href="http://www.25serviconotarial.com.br/contato-agradecimento.aspx" />
    <link href="<%=ConfigurationManager.AppSettings("bsHtml") %>css/contato.css" rel="stylesheet"
        type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>
        Agradecimento</h1>
    <p class="tx_contato" style="height:200px;">
        Obrigado por entrar em contato com o <strong>25º Tabelião de Notas Milani</strong>.<br />
        Em breve retornaremos sua solicitação de contato.
    </p>
</asp:Content>
